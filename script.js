let newTask = document.querySelector('.input-box');
let submit = document.querySelector('#add-btn');
let todoul = document.querySelector('.items-in');
let completeUl = document.querySelector('.complete-list ul');

let createTask = function (task) {
    let listItem = document.createElement('li');
    let checkbox = document.createElement('input');
    let label = document.createElement('label');

    listItem.className = 'item d-flex';
    label.innerText = task;
    checkbox.className = 'form-control incomplete-task';
    checkbox.type = 'checkbox';

    listItem.appendChild(checkbox);
    listItem.appendChild(label);

    return listItem;
}

let addTask = function (event) {
    event.preventDefault();
    let listItem = createTask(newTask.value);
    console.log('Hello: ', listItem);
    todoul.appendChild(listItem);
    newTask.value = "";
    bindInCompleteItems(listItem, completeTask);
}

let completeTask = function () {
    let listItem = this.parentNode;
    console.log('li:', listItem);
    let deleteBtn = document.createElement('button');
    deleteBtn.innerText = 'Delete';
    deleteBtn.className = 'btn btn-danger';
    deleteBtn.id = 'btn-delete';
    deleteBtn.type = 'button';
    listItem.appendChild(deleteBtn);

    let checkbox = document.querySelector('input[type="checkbox"]');
    console.log(checkbox);
    checkbox.remove();
    completeUl.appendChild(listItem);
    bindCompleteItems(listItem, deleteTask);
}

let deleteTask = function () {
    let listItem = this.parentNode;
    let ul = listItem.parentNode;
    ul.removeChild(listItem);
}

let bindInCompleteItems = function (taskItem, checkboxClick) {
    let checkbox = taskItem.querySelector('input[type = "checkbox"]');
    checkbox.onchange = checkboxClick;
}

let bindCompleteItems = function (taskItem, deleteButtonClick) {
    let deleteBtn = taskItem.querySelector('#btn-delete');
    deleteBtn.onclick = deleteButtonClick;
}

for(let i = 0; i<completeUl.children.length; i++){
    bindCompleteItems(completeUl.children[i], deleteTask);
}

for(let i = 0; i<todoul.children.length; i++){
    bindInCompleteItems(todoul.children[i], completeTask);
}

submit.addEventListener('click',addTask);